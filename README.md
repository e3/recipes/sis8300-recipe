# sis8300 conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/sis8300

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sis8300 module
